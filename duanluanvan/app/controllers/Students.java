package controllers;
import java.util.List;
import play.mvc.Result;
import models.Student;
import play.mvc.Controller;
import views.html.*;
import views.html.students.list;
import java.util.ArrayList;
import play.data.Form;
import views.html.students.details;

public class Students extends Controller{
	private static final Form<Student> studentForm = Form.form(Student.class);
	public static Result list(){
		List<Student> students= Student.findAll();
		return ok(list.render(students));
	}
	 public static Result newStudent() {
	        return ok(details.render(studentForm));
	    }
	public static Result details(String stt){
		final Student student = Student.findByStt(stt);
	    if (student == null) {
	      return notFound(String.format("Student %s does not exist.", stt));
	    }
	    Form<Student> filledForm = studentForm.fill(student);
	    return ok(details.render(filledForm));
	  }
	public static Result delete(String stt) {
		  final Student student = Student.findByStt(stt);
		  if(student == null) {
		    return notFound(String.format("Student %s does not exists.", stt));
		  }
		  Student.remove(student);
		  return redirect(routes.Students.list());

		}
	public static Result save(){
		Form<Student> boundForm = studentForm.bindFromRequest();
		if ( boundForm.hasErrors()){
			flash("error", "please correct the form below.");
			return badRequest(details.render(boundForm));
		}
		Student student = boundForm.get();
	    student.save();
	    flash("success", String.format("Successfully added product %s", student));
	    return redirect (routes.Students.list());
		}
}
