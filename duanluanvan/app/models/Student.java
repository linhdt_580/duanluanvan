package models;
import java.util.ArrayList;
import java.util.List;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
public class Student extends Model{
	@Id
	public Long id;
  @Constraints.Required
  public String stt;
	@Constraints.Required  
  public String name;
	@Constraints.Required
  public String ngaysinh;
	@Constraints.Required
  public String email; 
	@Constraints.Required
  public String quequan;
	
  public Student() {}

  public Student(String stt, String name, String ngaysinh, String email, String quequan) {

    this.stt = stt;
    this.name = name;
    this.ngaysinh = ngaysinh;     
    this.email=email;    
    this.quequan= quequan;

  }
  private static List<Student> students;
  static{
	  students = new ArrayList<Student>();
	  students.add(new Student("1","dangthanhlinh","20/05/1995","linhdt","thanhhoa"));
	  students.add(new Student("2","levanhung","20/10/1995","hunglv","vinhphuc"));
  }
  public static List<Student> findAll(){
	  return new ArrayList<Student>(students);
	  
  }
  public static Student findByStt(String stt){
	  for (Student candidate : students){
		  if (candidate.stt.equals(stt)){
		  	return candidate;
	  }
  }
	  return null;
  }
  public static List<Student> findByName(String term ){
	  final List<Student> results =new ArrayList<Student>();
	  for ( Student candidate: students){
		  if ( candidate.name.toLowerCase().contains(term.toLowerCase())){
			  results.add(candidate);
			  
		  }
	  }
	  return results;
  }
  public static boolean remove( Student student){
	  return students.remove(student);
  }
  public void save(){
	  students.remove(findByStt(stt));
	  students.add(this);
  }
  public String toString() {

    return String.format("%s - %s - %s - %s - %s", stt, name, ngaysinh, email, quequan);

  }

}